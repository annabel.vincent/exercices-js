// Event listener resize
window.onload = () => {
  init();
  window.addEventListener("resize", init, false);
};

// déclarer / créer les éléments
let video = document.createElement("video");
let controls = document.createElement("div");
let containerbar = document.createElement("div");
let playBtn = document.createElement("img");
let muteBtn = document.createElement("img");
let canvas = document.createElement("canvas");
let body = document.getElementsByTagName("body")[0];

// Configurer les éléments

// video
video.id = "video";
video.autoplay = true;
video.muted = true;
video.style.visibility = "hidden";
video.style.position = "absolute";

// Barre boutons de crontrôle
controls.className = "controls";
controls.style.width = "150px";
controls.style.height = "70px";
controls.style.textAlign = "center";
controls.style.backgroundColor = "#808080";
controls.style.opacity = 0.7;
controls.style.margin = "0 auto";
controls.style.borderRadius = "20px";

// Conteneur barre de conrtrôle
containerbar.className = "containerbar";
containerbar.style.position = "absolute";
containerbar.style.width = "100%";
containerbar.style.bottom = "10px";

// Boutons play et pause
playBtn.src = "../icons/pause.png";
playBtn.width = "50";
playBtn.height = "50";
playBtn.style.padding = "10px";
playBtn.addEventListener("click", function () {
  if (video.paused) {
    video.play();
    playBtn.src = "../icons/pause.png";
  } else {
    video.pause();
    playBtn.src = "../icons/play.png";
  }
});

// Boutons mute et unmute
muteBtn.src = "../icons/unmute.png";
muteBtn.width = "50";
muteBtn.height = "50";
muteBtn.style.padding = "10px";
muteBtn.addEventListener("click", function () {
  if (video.muted) {
    video.muted = false;
    muteBtn.src = "../icons/mute.png";
  } else {
    video.muted = true;
    muteBtn.src = "../icons/unmute.png";
  }
});

// Canvas
canvas.id = "canvas";
canvas.style.display = "block";
canvas.style.border = "no";

// Ajout des boutons dans la barre de contrôle
controls.appendChild(playBtn);
controls.appendChild(muteBtn);

// Ajout de la barre dans le conteneur
containerbar.appendChild(controls);

// Ajout des éléments au body
body.appendChild(video);
body.appendChild(containerbar);
body.appendChild(canvas);

// Dessiner la vidéo dans le canvas au chargement et en boucle
function init() {
  let canvas = document.getElementById("canvas");
  let ctx = canvas.getContext("2d");
  let width = window.innerWidth;
  let height = window.innerHeight;

  canvas.width = width;
  canvas.height = height;

  ctx.drawImage(video, 0, 0, width, height);
  requestAnimationFrame(init);
}

// Régler la source de la vidéo
function setVideoSrc(src) {
  let video = document.getElementById("video");
  video.src = "../videos/" + src;
}

// Récupérer les paramètres de l'url
function getParamValue() {
  let url = window.location.search.substring(1);
  let array = url.split("=");
  let src = array[1];
  if (src !== undefined) {
    setVideoSrc(src);
  } else {
    setVideoSrc("../videos/video2.mp4");
  }
}

getParamValue();
