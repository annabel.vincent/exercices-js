let data = [6, 1, 4, 2, 5, 3];

// Trier les impairs à gauche et pairs à droite et les classer du plus petit au plus grand
data.sort((a, b) => (b % 2) - (a % 2) || (b - a ? a - b : a % 2));

console.log(data);
